import os
from typing_extensions import final
import numpy as np
from pandas.io.parsers import read_csv
from .config import use_neutral, confident, mask_neutral


os.environ["CUDA_VISIBLE_DEVICES"] = ""
# os.environ["OMP_NUM_THREADS"] = "1"
# os.environ["MKL_NUM_THREADS"] = "1"
import torch
# torch.set_num_threads(1)

from fastai.text import *
import multifit
import MeCab
mecab_tagger = MeCab.Tagger()


path = os.getcwd()

device = torch.device("cpu")


def load_learner(folder_name='save/hotel-review-multifit', model_folder='multifit_paper_versionseed4'):
    # exp = multifit.from_pretrained('ja_multifit_paper_version')
    exp = multifit.configurations.multifit_paper_version().load_(Path(os.path.join(path, 'save/ja_multifit_paper_version')))
    # print('111111111111111111111111111111111111111;,',os.path.join(path, folder_name))
    # tokenizer = exp.arch.new_tokenizer(Path(os.path.join(path, folder_name, 'models/sp15k/spm.model')))
    tokenizer = exp.pretrain_lm.tokenizer
    cls_dataset = exp.arch.dataset(Path(os.path.join(path, folder_name)), tokenizer)
    data_clas = cls_dataset.load_clas_databunch(bs=exp.finetune_lm.bs)
    # exp.load_(Path('/mnt/aime/Documents/tan/sentiment_ja/data/models/sp15k/multifit_paper_version'))

    learner = exp.classifier.get_learner(data_clas, eval_only=True)
    learner_path = [os.path.join(path, folder_name, 'models/sp15k', i, 'cls_best') for i in
                        os.listdir(os.path.join(path, folder_name, 'models/sp15k')) if i.startswith(model_folder)]

    learner.load(Path(learner_path[0]), device=device)
    learner.model.eval()

    return learner


def predict_one_learner(learner, sentence):
    with torch.no_grad():
        res = learner.predict(sentence)
        score = res[2].tolist()
    return score


learner_hotel_review = load_learner()
learner_real_data = load_learner('save/real-data-multifit', 'multifit_paper_versionseed2')
final_mapping = ['positive', 'negative', 'neutral']
if not use_neutral:
    final_mapping = ['positive', 'negative']
    confident = 1
    mask_neutral = 0

hotel_review_mapping = ['positive', 'negative', 'positive']
real_data_mapping = ['positive', 'negative', 'positive', 'neutral']


# def predict(sentence):
#     score = predict_one_learner(learner_real_data, sentence)
#     label = real_data_mapping[np.argmax(score)]
#     return label, score


def softmax(score):
    exp = np.exp(score)
    return (exp / sum(exp)).tolist()

original_form_pos = 3
type_pos = 4
# adj_japan = '形容詞'
noun_japan = '名詞'

pos_adjs = '美味しい|おいしい|オイシイ|うまい|ウマい|美味い|やさしい|優しい|親切|明るい|気持ち良い|気持ちよい|広い|心地よい|可愛い|好もしい|好き|楽しい|たのしい|ここちよい|程よい|笑顔|便利|充実|偉い|暖かい|あったかい|温かい|美しい|綺麗|きれい|キレイ|新しい|あたらしい|良い|可愛い|かわいい|可愛らしい|見易い|かっこよい|すばらしい|素晴らしい|素敵|すてき|カッコイイ|礼儀正しい|仲良い|清潔|すばやい|上手|すごい|印象深い|分かり易い|嬉しい|感謝|ありがたい|有難い|お世話になりました|ほほえましい|微笑ましい|安い|安全|相応しい|快適|満足|質が良い|近い|丁寧|ていねい'
neg_adjs = 'キタナイ|汚い|汚れ|きたない|マズイ|まずい|マズい|不味い|怖い|うるさい|煩い|騒々しい|危ない|狭い|古い|気持ち悪い|かゆい|痒い|めんどい|めんどくさい|面倒|邪魔|腹立たしい|つまらない|しんどい|しつこい|臭い|面倒臭い|物足りない|きびしい|厳しい|激しい|せせこましい|堅い|かたい|気味が悪い|さむい|じゃまくさい|失礼|暗い|荒っぽい|痛い|埃っぽい|不便|心配|悩|くだらない|寂しい|悲しい|残念|重い|難しい|つらい|見づらい|おどろおどろしい|ややこしい|遠い|遅い|乏しい|蒸し暑い|騒がしい|眩しい|まぶしい|やかましい|生臭い|不良'

type_of_adjectives = {i: 'positive' for i in pos_adjs.split('|')}
type_of_adjectives.update({i: 'negative' for i in neg_adjs.split('|')})

punct = set(list('！ ” ＃ ＄ ％ ＆ ’ （ ） ＊ ＋ 、 − ． ／ ： ； ＜ ＝ ＞ ？ ＠ ＼ ＾ ＿ ｀ ｜ 〜[,.、。!・？?「」"~() ') + ["'"])



def mecab_tag(text):
    """
    return list of (word, original form, type)
    """
    res = []
    for line in mecab_tagger.parse(text).split('\n')[:-2]:
        a = line.split('\t')
        res.append([a[0], a[original_form_pos], a[type_pos].split('-')[0]])
    return res

def rule1(text):
    tagged_text = mecab_tag(text)
    if len(tagged_text) < 4:
        return None
    
    label = None
    text0 = tagged_text[1][0]
    text1 = tagged_text[3][0]
    if tagged_text[0][2] == noun_japan and text0 in ['は', 'が', 'も'] \
        and text1 in ['たです', 'です', 'だった', 'だ'] and tagged_text[2][1] in type_of_adjectives \
        and (len(tagged_text) == 4 or tagged_text[-1][0] in punct):
        label = type_of_adjectives[tagged_text[2][1]]
    return label 


def predict(sentence, get_all_score=False, get_raw_score=False, normalize_before_add=False, rule=rule1):
    if not use_neutral:
        score = {'positive': 0, 'negative': 0}
    else:
        score = {'positive': 0, 'negative': 0, 'neutral': 0}
    if rule is not None:
        label = rule(sentence)
        if label is not None:
            score[label] = 1.0
            return label, score
    if use_neutral:
        hotel_review_model_score = predict_one_learner(learner_hotel_review, sentence)
        real_data_model_score = predict_one_learner(learner_real_data, sentence)

        # hotel_review_model_score[0] = max(hotel_review_model_score[0], hotel_review_model_score[2])
        hotel_review_model_score[0] = hotel_review_model_score[2]
        hotel_review_model_score[2] = real_data_model_score[3]

        # real_data_model_score[0] = max(real_data_model_score[0], real_data_model_score[2])
        real_data_model_score[0] = real_data_model_score[2]
        real_data_model_score[2] = real_data_model_score[3]

        if normalize_before_add:
            real_data_model_score = softmax(real_data_model_score)

            hotel_review_model_score = softmax(hotel_review_model_score[:2])
            hotel_review_model_score.append(real_data_model_score[2])

        real_data_model_score = np.array(real_data_model_score[:-1])    
        hotel_review_model_score = np.array(hotel_review_model_score)

        final_score = confident * hotel_review_model_score + (1-confident) * real_data_model_score
        final_score[2] = mask_neutral * final_score[2]

        if not get_raw_score:
            final_score = softmax(final_score)
        label = final_mapping[np.argmax(final_score)]
        score = {final_mapping[i]: final_score[i] for i in range(len(final_mapping))}

        if get_all_score:
            hotel_review_model_score[2] = 0
            hotel_review_model_score = {final_mapping[i]: hotel_review_model_score[i] for i in range(len(final_mapping))}
            real_data_model_score = {final_mapping[i]: real_data_model_score[i] for i in range(len(final_mapping))}
            return label, score, hotel_review_model_score, real_data_model_score
        

        return label, score
    else:
        hotel_review_model_score = predict_one_learner(learner_hotel_review, sentence)
        # hotel_review_model_score[0] = max(hotel_review_model_score[0], hotel_review_model_score[2])
        hotel_review_model_score[0] = hotel_review_model_score[2]

        final_score = hotel_review_model_score[:2]
        if not get_raw_score:
            final_score = softmax(final_score)
        label = final_mapping[np.argmax(final_score)]
        score = {final_mapping[i]: final_score[i] for i in range(len(final_mapping))}

        return label, score


