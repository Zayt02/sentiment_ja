use_neutral = True  # using the real data model (with neutral label)

# Settings below are useless if use_neutral = False
confident = 0.9 # confident is the weight of hotel review model, (1-confident) is for real data model
mask_neutral = 0.5 # mask the neutral probability by multiply mask_neutral

